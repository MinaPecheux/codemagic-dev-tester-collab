# [Unity/C#] Dev/tester collab with Unity and Codemagic

**Mina Pêcheux - June 2022**

![Codemagic build status](https://api.codemagic.io/apps/62bc130f295ddcafdc606c88/unity-win-workflow/status_badge.svg)

This project is a small sample to show [Codemagic](https://unitycicd.com/)'s sharing features and QA/testing tools that make sending your new game builds to the right teammates way easier!
