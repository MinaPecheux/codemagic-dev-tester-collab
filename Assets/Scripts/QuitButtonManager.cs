using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;      
#endif

public class QuitButtonManager : MonoBehaviour
{
    void Start()
    {
        GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => {
#if UNITY_EDITOR
            EditorApplication.ExitPlaymode();
#else
            Application.Quit();
#endif
        });
    }
}
